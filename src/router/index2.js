import { createRouter, createWebHistory } from "vue-router";
// import header from "../components/luma/header.vue";
import Home from "../components/luma/Home";
import Category from "../components/luma/Category";
import ProductDetail from "../components/luma/ProductDetails";
const routes = [
  {
    path: '/home',
    component: Home,
    name: "Home",
  },
  {
    path: '/category',
    component: Category,
    name: "Category",
  },
  {
    path: '/product-details',
    component: ProductDetail,
    name: "ProductDetail",
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes:routes,
})

export default router
