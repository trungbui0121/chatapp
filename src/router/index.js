import { createRouter, createWebHistory } from "vue-router";
import Login from "../components/auth/Login.vue";
import Register from "../components/auth/Register.vue";
import Profile from '../components/profile/index.vue'
import Page1 from '../components/PageApp/page1.vue'
import AppChat from '../components/PageApp/AppChat.vue'
import AppOfAppChat from '../components/PageApp/AppOfAppChat.vue'
import store from '../store/index'
const routes = [

  {
    path: '/login',
    component: Login,
    name: "Login",
    meta: { guest: true }
  },
  {
    path: '/register',
    component: Register,
    name: 'Register',
    meta: { guest: true }
  },
  {
     path: '/profile',
     component: Profile,
     name: 'Profile'
  },
  {
    path: '/page1',
    component: Page1,
    name: 'Page1',
    meta: { auth: true }
 },
 {
   path: '/',
   component: AppChat,
   name: 'AppChat',
   meta: { auth: true }
 },
 {
  path: '/AppOfAppChat',
  component: AppOfAppChat,
  name: 'AppOfAppChat',
  // meta: { auth: true }
}
];

const router = createRouter({
  history: createWebHistory(),
  routes:routes,
})
//beforeeach truoc moi router no check..goi trc kh chuyen huong
//aftereach sau khi chuyen huong se thuc hien ham
//beresolve khi ng dung chuyen huong  chay sau befo va trc after

router.beforeEach((to, from) => {
  console.log(to)
  console.log(from)
  
  if (to.meta.auth) {
    if (!store.getters.authenticated) {
      return { name: 'Login' }
    }
  }
  if (to.meta.guest) {
    if (store.getters.authenticated) {
      return { name: 'AppChat'}
    }
  }
  return true
  

})
export default router
