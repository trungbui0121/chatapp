import { createStore } from 'vuex'
import authModule from './modules/auth'
const store = createStore({
    modules: {
        authModule
    },
    // strict: process.env.NODE_ENV == 'production'
    // strict: !import.meta.env.PROD

})
export default store