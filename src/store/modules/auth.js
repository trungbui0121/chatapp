import { LOGIN } from './../action-types'
import { SET_AUTH, SET_USER, LOGOUT } from './../mutation-types'

export default {
    state: {
        authenticated: false,//xd ng dung dang nhap chua
        user: {},
        token: ''
    },
    actions: {
        [LOGIN]({ commit }, data)  {
            commit(SET_AUTH, data.token)
            commit(SET_USER, data.user)
        },
    },
    mutations: {
        [SET_AUTH](state, token) { //[setauth] cach dat ten trong js
            state.authenticated = true
            state.token = token
        },
        [SET_USER](state, user){
            state.user = user
        },
        [LOGOUT](state) {
            state.authenticated = false
            state.user = {}
            state.token = ''
        }
    },
    getters: {
        authenticated: (state) => state.authenticated,     
    }
}