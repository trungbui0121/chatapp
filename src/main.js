import { createApp } from 'vue'
import Layout from './Layout.vue'
import { initializeApp } from "firebase/app";
import './router/index'
import router from './router/index'
import store from './store'
import { LOGIN } from './store/action-types'
import authStorage from './storage/auth.storage'
import 'bootstrap/dist/css/bootstrap.css'

//lay storage de xac thuc dang nhap neu da dang nhap thi vao trang luon
const authStorageData = authStorage.getToken()
if (authStorageData.user && authStorageData.access_token) {
    store.dispatch(LOGIN, {
        token: authStorageData.access_token,
        user: authStorageData.user
    })
}
//end
const firebaseConfig = {
    apiKey: "AIzaSyAjEmGTqL3LI_g8jhoeFhsPpiz_fJnoGF4",
    authDomain: "appvify.firebaseapp.com",
    databaseURL: "https://appvify-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "appvify",
    storageBucket: "appvify.appspot.com",
    messagingSenderId: "509404352488",
    appId: "1:509404352488:web:2ddbd14138913cfdc1de5d"
};

const app = createApp(Layout)
app.config.globalProperties.$firebase = initializeApp(firebaseConfig)
app.use(router).use(store).mount('#app')