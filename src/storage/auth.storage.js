export default {
    setAuth(token, user) {
        window.localStorage.setItem('access_token', token)
        window.localStorage.setItem('user', JSON.stringify(user)) 
    },
    getToken(){
        const user = window.localStorage.getItem('user')
        return {
            access_token : window.localStorage.getItem('access_token'),
            user : user ? JSON.parse(user) : null
        }
    },
    removeToken() {
        window.localStorage.removeItem('access_token')
        window.localStorage.removeItem('user')
    },  
}
//JSON VA PARSE DE LUU DU LIEU DANG DOI TUONG DUOC VAO LOCAL VI LOCAL CHI LUU DC DANG CHUOI